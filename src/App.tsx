import React, {useState, useEffect} from 'react';
import Board from './Board';
import Snake from './Snake';
import Food from './Food';
import styled from "styled-components";

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 50px;
`

const App: React.FC<{}> = () => {
  const [direction, setDirection] = useState<string>("");
  const [foodPosition, setFoodPosition] = useState([0,0]);

  useEffect(()=> {
    const getKey = (e:KeyboardEvent) => {
      switch(e.key){
        case "w":
        case "ArrowUp":
          if(direction !== "down"){
            setDirection("up")
          }
          break;
        case "s":
        case "ArrowDown":
          if(direction  !== "up"){
            setDirection("down")
          }
          break;
        case "a":
        case "ArrowLeft":
          if(direction !== "right"){
            setDirection("left")
          }
          break;
        case "d":
        case "ArrowRight":
          if(direction !== "left"){
            setDirection("right")
          }
          break;
        //FOR DEBUG
        case " ":
          setDirection("")
        break;
        default:
          break;
      }
    }
    document.addEventListener("keydown", getKey)
    return () => {
      document.removeEventListener("keydown", getKey)
    }
  },[direction])
  return (
    <Container>
      <Board>
        <Snake direction={direction} food={foodPosition} setFoodPosition={setFoodPosition} />
        <Food foodPosition={foodPosition} setFoodPosition={setFoodPosition}/>
      </Board>
    </Container>
  );
}

export default App;
