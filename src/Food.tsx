import React, {useState, useEffect} from 'react';
import styled from "styled-components";

const FoodStyle = styled.div<{position: number[]}>`
	left: ${props =>  `${props.position[0] * 40}px`};
	top: ${props =>  `${props.position[1] * 40}px`};
	background: red;
	width: 35px;
	height: 35px;
	border: 5px solid #011627;
	border-radius: 50em;
	position: absolute;
`

const Food: React.FC<{foodPosition: number[], setFoodPosition: Function}> = ({foodPosition, setFoodPosition}) => {

	useEffect(() => {
		const generatePosition = ():number => {
			return Math.floor(Math.random() * 20);
		}
		const xPosition = generatePosition();
		const yPosition = generatePosition();
			setFoodPosition([xPosition, yPosition])
	}, [setFoodPosition])

	return <FoodStyle position={foodPosition} />
}

export default Food;