import React from "react";
import styled from "styled-components";

/* 800 / 40 = 20 Segment Board */
const BOARD_DIMENSION = 805;

const BoardStyle = styled.div`
  width: ${`${BOARD_DIMENSION}px`};
  height: ${`${BOARD_DIMENSION}px`};
  background: #011627;
  position: relative;
`;

const Board: React.FC<{}> = ({ children }) => {
  return <BoardStyle>{children}</BoardStyle>;
};

export default Board;
